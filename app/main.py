#! /bin/env python3

import logging
import json
from os import environ

from telegram.ext import CommandHandler, MessageHandler, Updater, PicklePersistence, Filters

import text_commands
import event_handlers


# logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO,
                    filename="logs/app/info")



# authentication and proxy
proxy_url:str
try:
    proxy_url = environ['TASKBOT_PROXY']
except KeyError:
    try:
        with open("secrets/proxy") as proxyfile:
            proxy_url = proxyfile.readline().strip()
    except FileNotFoundError:
        proxy_url = "" # no proxy configured

try:
    telegram_bot_token = environ['TASKBOT_TOKEN']
except KeyError:
    try:
        with open("secrets/telegram_token_test", "r") as tokenfile:
            telegram_bot_token = tokenfile.readline().strip()
    except FileNotFoundError:
        #TODO log, STDERR
        exit(-1)

# persistence
persistence_path:str
try:
    persistence_path = environ['TASKBOT_PERSISTENCE_FILE']
except KeyError:
    try:
        with open("app/config.json") as config:
            persistence_path = json.load(config)["persistence_path"]
    except FileNotFoundError:
        persistence_path = "./persistence"
finally:
    persistence = PicklePersistence(filename=persistence_path)
    #TODO why there is a database AND persistence?

updater_request_kwargs = {'proxy_url': proxy_url} if proxy_url else dict()
updater = Updater(token=telegram_bot_token, request_kwargs=updater_request_kwargs, persistence=persistence)
del telegram_bot_token
del proxy_url


# event listeners
updater.dispatcher.add_handler(
    MessageHandler(Filters.all, event_handlers.keep_user_register ),
    group=0)


# generate text commands
for function_name, function in text_commands.get_commands().items() :
    updater.dispatcher.add_handler(
        CommandHandler(
            function_name, function, 
            pass_args=True, pass_user_data=True, pass_chat_data=True
        ),
        group=2)


# bot run
if __name__ == "__main__":
    updater.start_polling()
    updater.idle()
