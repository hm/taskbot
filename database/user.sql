-- db with informations about the telegram user

PRAGMA foreign_keys = ON; 

BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS users (
    username TEXT, 
    firstname TEXT,
    lastname TEXT,
    object BLOB
    );

COMMIT;
