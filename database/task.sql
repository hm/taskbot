-- db for public discolsure without personal informations

PRAGMA foreign_keys = ON; 

BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS task (
    id INTEGER PRIMARY KEY,  
    creation_timestamp TEXT DEFAULT CURRENT_TIMESTAMP, -- all creation times refer to the moment of the insertion in the db
    creation_time TEXT DEFAULT CURRENT_TIME,
    creation_date TEXT DEFAULT CURRENT_DATE,
    creation_message_text TEXT NOT NULL, -- full text message, same as description
    creation_message_id INT NOT NULL,
    creation_chat_id INT NOT NULL, -- used even to identify the place
    creation_request_by_id INT NOT NULL,
    creation_confirm_message_id INT,
    creation_confirm_message_text TEXT,
    deleted BOOL DEFAULT FALSE,
    deletion_timestamp TEXT,
    deletion_time TEXT,
    deletion_date TEXT,
    deletion_message_text TEXT,
    deletion_message_id INT,
    deletion_chat_id INT,
    deletion_request_by_id INT, -- the telegram id requesting the deletion
    type TEXT NOT NULL,
    place TEXT
--    FOREIGN KEY (place) REFERENCES places_group_id (place)
    );

CREATE TABLE IF NOT EXISTS participants (
    task_id INT,
    telegram_id INT,
    FOREIGN KEY (task_id) REFERENCES task (id)
    );

CREATE TABLE IF NOT EXISTS places_group_id (
    place TEXT NOT NULL,
    inserted TEXT DEFAULT CURRENT_TIMESTAMP, -- group has migrated, only last timestamp is considered
    telegram_group_id INT NOT NULL
    );

CREATE TABLE IF NOT EXISTS tg_account (
    tg_id INTEGER,
    disabled BOOL DEFAULT FALSE,
    disabled_timestamp TEXT,
    registration_timestamp TEXT DEFAULT CURRENT_TIMESTAMP
    ); --TODO date on changed?
    -- TODO TRIGGER tg_ids not disabled must be unique


--TODO:
--  TABLE bounties for requested tasks
--  creation_chat_id in TABLE task should be a FK on places_group_id
CREATE TABLE IF NOT EXISTS bounties (
    id INT PRIMARY KEY,
    creation_timestamp TEXT DEFAULT CURRENT_TIMESTAMP, -- all creation times refer to the moment of the insertion in the db
    creation_time TEXT DEFAULT CURRENT_TIME,
    creation_date TEXT DEFAULT CURRENT_DATE,
    creation_message_text TEXT NOT NULL, -- full text message, same as description
    creation_message_id INT NOT NULL,
    creation_chat_id INT NOT NULL, -- used even to identify the place
    creation_request_by_id INT NOT NULL,
    creation_confirm_message_id INT,
    creation_confirm_message_text TEXT,
    deleted BOOL DEFAULT FALSE,
    deletion_timestamp TEXT,
    deletion_time TEXT,
    deletion_date TEXT,
    deletion_message_text TEXT,
    deletion_message_id INT,
    deletion_chat_id INT,
    deletion_request_by_id INT, -- the telegram id requesting the deletion
    type TEXT NOT NULL,
    place TEXT,
    completed_task_id INT, -- number of the task corresponding to the bounty
    FOREIGN KEY (completed_task_id) REFERENCES task (id)
    );

COMMIT;    