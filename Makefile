
.DEFAULT_GOAL := help
.PHONY: clean pep8 black lint test install

PYLINT          := pylint
PYTEST          := pytest
PEP8            := flake8
BLACK           := black
MYPY            := python -m mypy
PIP             := python -m pip
PIPENV			:= pipenv
RDBMS			:= sqlite3
TASK_DB			:= task
DB_SUFFIX		:= sqlite3

clean:
	rm -fr build
	rm -fr dist
	find . -name '*.pyc' -exec rm -f {} \;
	find . -name '*.pyo' -exec rm -f {} \;
	find . -name '*~' -exec rm -f {} \;
	#find . -regex "./telegram[0-9]*.\(jpg\|mp3\|mp4\|ogg\|png\|webp\)" -exec rm {} \;

pep8:
	$(PEP8) telegram 

black:
	$(BLACK) .

lint:
	$(PYLINT) --rcfile=setup.cfg app

mypy:
	$(MYPY) app

test:
	$(PYTEST) -v

install:
	# $(PIP)  install -r requirements.txt -r requirements-dev.txt
	$(PIPENV) install

# help:
# 	#TODO
# 	@echo "Available targets:"
# 	@echo "- clean       	Clean up the source directory"
# 	@echo "- pep8        	Check style with flake8"
# 	@echo "- lint        	Check style with pylint"
# 	@echo "- black       	Check style with black"
# 	@echo "- mypy        	Check type hinting with mypy"
# 	@echo "- test        	Run tests using pytest"
# 	@echo "- ensure_pipenv
# 	@echo
# 	@echo "Available variables:"
# 	@echo "- PYLINT      	default: $(PYLINT)"
# 	@echo "- PYTEST      	default: $(PYTEST)"
# 	@echo "- PEP8        	default: $(PEP8)"
# 	@echo "- BLACK       	default: $(BLACK)"
# 	@echo "- MYPY        	default: $(MYPY)"
# 	@echo "- PIP         	default: $(PIP)"
# 	@echo "- PIPENV      	default: $(PIPENV)"
 
pipenv:
	@echo "pipenv already installed in `which pipenv`, pip already installed in `which pip`. Nothing to do." || \
	echo "installing pipenv" && python -m ensurepip && python -m pip install --upgrade pipenv;
	

db:
	#if not exist generate task.sqlite3 database based on schema
	sqlite3 database/$(TASK_DB).$(DB_SUFFIX) '.read database/$(TASK_DB).sql'

reset_db:
	mv database/$(TASK_DB).$(DB_SUFFIX) database/$(TASK_DB).$(DB_SUFFIX).$(date -u +"%Y-%m-%d")
	sqlite3 database/$(TASK_DB).$(DB_SUFFIX) '.read database/$(TASK_DB).sql'

run:
	#TODO (supervisor)

del_db_exports:
	rm -rf database/*
	
del_logfiles:
	rm -rf logs/app/*
	rm -rf logs/database/*

free_space: del_db_exports, del_logfiles