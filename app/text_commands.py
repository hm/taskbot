"""Collection of all text commands (commands that starts with / ). 
This module is the only message interface: other modules should not communicate with the telegram user.
Decorate a function with @command for marking it as a text command.
Functions here should be only for user interaction (messages etc).
"""
import time 
from functools import wraps
from itertools import repeat
from sqlite3 import Error as DBError
from textwrap import dedent
from typing import Union, Optional, List, Dict, Set, Tuple # Literal
import json
from pprint import pprint

from telegram import Chat, ChatMember, Message, MessageEntity, Update, User, ChatAction
from telegram.ext import CallbackContext, Filters

import helpers as h
import event_handlers
from constants import ANY_MENTION, PERIODS, USERS, CONFIG_FILE_PATH


# flags and conf
with open(CONFIG_FILE_PATH) as config_file:
    config = json.load(config_file)
development = config["development"]

with open("secrets/dev_telegram_ids") as dev_ids_file:
    dev_ids = map(int, dev_ids_file.readlines())

# decorator functions
__command_dict__ = dict()


def command(func, command_name: str = ""):
    """functions decorated will be counted as '/' commands"""
    if not command_name:
        command_name = func.__name__
    global __command_dict__
    __command_dict__[command_name] = func
    
    #@wraps(func)
    #def inner(func):
    #    return func
    
    return func


def get_commands() -> dict:
    return __command_dict__
 

def nullfunc(*, phony):
    return


def admin_only(func): # decorator
    """decorator: the command is only to be used by admins. it implies it is only in groups"""
    @wraps(func)
    def inner(update: Update, context: CallbackContext) -> None:
        user: User = update.effective_user
        chat: Chat = update.effective_chat
        if h.is_admin(user, chat) and h.is_group(chat):
            return func
        else:
            return nullfunc
        
    return inner


def dev_only(func): # decorator
    """decorator: the command is only to be used by developers"""
    @wraps(func)
    def inner(update: Update, context: CallbackContext) -> None:
        user: User = update.effective_user
        if user.id in dev_ids:
            return func
        else:
            return nullfunc
        
    return inner


def group_only(func): # decorator
    """decorator: the command is only to be used in groups"""
    @wraps(func)
    def inner(update: Update, context: CallbackContext) -> None:
        if h.is_group(update.effective_chat):
            return func
        else:
            return nullfunc
        
    return inner


def private_only(func): # decorator
    """decorator: the command is only to be used in private chats"""
    @wraps(func)
    def inner(update: Update, context: CallbackContext) -> None:
        if h.is_private(update.effective_chat):
            return func
        else:
            return nullfunc
        
    return inner


def test_only(func): # decorator
    """decorator: the command will be available only durong development""" 
    if development:
        return func
    else:
        return nullfunc


def is_from_admin(update:Update) -> bool:
    """Admin only restriction used for early returns. 
    This is a wrapper with error message."""
    if not h.is_from_admin(update.message): 
        update.message.reply_text("Nothing done: admin only.")
        return False
    return True


@command
def export_db(update: Update, context: CallbackContext) -> None:
    """exports db"""
    path_dir = "database/exports/" #TODO put in config file
    
    sql_args = dict(zip(["sql", "sqlite", "sqlite3", "db", "database"], repeat("sql"))) # maps all to "sql"
    xls_args = dict(zip(["xls", "xlsx", "excel", "excell", "spreadsheet"], repeat("xlsx"))) 
    # odf_args = dict(zip(["odf", 'libreoffice', 'open'], repeat('odf'))) #TODO add open formats 
    all_args = {**sql_args, **xls_args} # **odf_args}
    
    epoch_timestamp = str(int(time.time()))
    file_name_without_extension = "db_export_" + epoch_timestamp  
    
    try: #test valid selection
        file_format = all_args[context.args[0].lower()]
    except KeyError: # invalid selection
        update.message.reply_text(
            f"Invalid command: try '/export_db' followed by one of: {', '.join(all_args)}. \nNothing done.")
        #TODO logging
        return
    
    file_name = file_name_without_extension + '.' + file_format
    file_path = path_dir + file_name
    h.export_db(file_path, file_format) # file operation
    with open(file_path, mode="rb") as file_to_send:
        context.bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.UPLOAD_DOCUMENT)
        update.message.reply_document(document=file_to_send, filename=file_name, quote=True)    

@command
def ping(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("pong")

@command
def people(update: Update, context: CallbackContext) -> None:
    """show registered people"""
    update.message.reply_text(context.bot_data[USERS].to_json())
    

@command
def stats(update: Update, context: CallbackContext) -> None:
    """stats about task done in a specific timeframe
    
    /stats topN [[day | today] | yesterday | Ndays | daysN |
                    [week | thisweek] | pastweek | Nweeks | weeksN |
                    [month | thismonth] | pastmonth | Nmonths | monthsN |
                    [year | thisyear] | pastyear |
                    [overall | alltimes]] 
                [bountyhunters]
        topN:   get only the N top contributors
        day:    """        #TODO user guide  
    
    error_parsing_args_replytext = dedent("\
        I cannot understand your request.  \n \
        Try using me like '/stats top5 10days' for getting \
        the top 5 contributors of the past 10 days.")  
    try:
        top_n = int(context.args[0].strip('top'))
        period_to_parse = context.args[1]
    except IndexError as e:
        update.message.reply_text(error_parsing_args_replytext)
        raise e
    try:
        only_bounties = 'bounty' in context.args[2]
    except IndexError: # not third arg
        only_bounties = False

    period = list(filter(lambda period: period in period_to_parse, PERIODS))[0] 
    digits: Optional[list] = list(filter(lambda char: char.isdigit(), period_to_parse))
    if 'past' in period_to_parse or 'yester' in period_to_parse:
        period_multiplicator = 1
    elif digits: # contains a number?
        period_multiplicator = int(''.join(map(lambda char: str(char), digits))) # concatenate numbers
    else: # today, thismonth, etc...
        period_multiplicator = 0
    
    result_str: Dict[str,str] = h.get_stats(top_n, period_multiplicator, period, only_bounties) # Dict[id, task_count]
    try:
        id_points_map: Dict[int,int] = h.dict_type_casting(result_str, int)
    except TypeError as e: # None, no results
        update.message.reply_text("No results.")
        raise e

    reply = f"Top {top_n} {'bounty hunters:' if only_bounties else ':'}"
    registered_users:Set[User] = context.bot_data[USERS]
    # username_user_map:Dict['str',User] = context.bot_data[USERNAME_TO_USEROBJ_MAP]
    # user_username_map:dict = h.reverse_dict(username_user_map)
    # id_user_map: dict = dict(map(lambda user: (user.id, user), user_username_map))
    id_user_map: dict = dict(map(lambda user: (user.id, user), registered_users)) 
    for user_id in id_points_map:
        user: User = id_user_map[user_id]
        reply += f"\n{user.mention_markdown_v2()}: {str(id_points_map[user_id])}" 
    
    update.message.reply_markdown_v2(reply)
    

@command
def help(update: Update, context: CallbackContext) -> None:
    usage = dedent("""
        Usage: use a command of (/use, /cleaning, /construction, /assistance, /meeting) for inserting a task. 
    
        The command must be followed by @mentions of all the participants.
        Except for that rule, you can write what you want in the message. Build your story!""")
    
    if "admin" in update.message.text:
        usage += dedent("""

        Admin-only commands:
        /delete: respond with that command to a message creating the task for deleting it
        /export_db [spreadsheet|sql]: export the current database in various formats
        /stats topX Ydays: show the top X contributors in the last Y days """)

    # if "dev" in update.message.text:
    #     usage += """

    #     Dev-only usage:
    #     /chat_number
    #     /test"""

    update.message.reply_text(dedent(usage))


## group task commands
def register_task(update: Update, context: CallbackContext, task_type: str) -> None:
    """generic function for task commands"""
    message = update.message

    mentions = h.get_mentions(message)
    participants = h.get_users(mentions, context)
    if participants:
        task_id = h.insert_task(update, context)

        success_text = f"OK, {task_type} task #{task_id} registered."  
        thanks = "\nThanks to " + ", ".join(user.mention_html() for user in participants) + "! " 
        reply_text = success_text + thanks
    else:
        reply_text = dedent("""
                            No participants assigned to the task.
                            Try mentioning @someone for adding they as a participant!

                            Nothing Done.
                            """).lstrip()
    update.message.reply_html(reply_text, quote=True)


@command #admin
def delete(update: Update, context: CallbackContext) -> None:
    """mark as deleted the task in the message replied_to"""
    if  not update.effective_user.id in dev_ids or \
            update.effective_chat.type in [Chat.GROUP, Chat.SUPERGROUP] and \
        not is_from_admin(update):
        return    

    message = update.message
    if message.reply_to_message: # it's a reply
        original_message: Message = message.reply_to_message
        deleted_task_id = h.delete_task(deleting_message=message, original_message=original_message)
    else: # not a reply, the second item in the string should be the task number to delete
        try:
            task_id_to_be_deleted = int(message.text.split()[1])
            deleted_task_id = h.delete_task(deleting_message=message.text, task_id=task_id_to_be_deleted)
        except ValueError: # not a number
            update.message.reply_text(dedent("""
                Task not deleted: try using '/delete 56 ...' for deleting a task 
                or comment to the message that created the task."""))
            return

    update.message.reply_text(f"Task {deleted_task_id} deleted" if deleted_task_id else "Task not deleted")


@command #admin
def register_group(update: Update, context: CallbackContext) -> None:
    """Admin can associate a group to a place name.
    Substituting the precedent group:place pair or creating another place.
    type this command followed by the name that you want to assign to the place. 
    Spaces and capitalization are ignored"""

    message = update.message
    place_name = message.text.split()[1].strip('#')
    group_id = update.effective_chat.id
    # check if it is a new place, in this case put a wanring #TBD
    existing_places = h.list_places()
    warning = ""
    if place_name not in existing_places:
        warning = dedent(f"""
            Warning: the place name you inserted, '{place_name}', does not exists yet. 
            The places currently registered were: {', '.join(existing_places) or "[no one]"}. 
            Inserting it anyway... \n""")

    h.register_group_as_place(group_id, place_name)

    #if success:
    reply_text = f"OK, current group registered as {place_name}."
    # else:
    #     reply_text = "Not registered. Nothing done"
    reply_text = warning + reply_text
    message.reply_text(reply_text)


@command
def assistance(update: Update, context: CallbackContext) -> None:
    register_task(update, context, "assistance")


@command
def construction(update: Update, context: CallbackContext) -> None:
    register_task(update, context, "construction")


@command
def use(update: Update, context: CallbackContext) -> None:
    register_task(update, context, "use")


@command
def cleaning(update: Update, context: CallbackContext) -> None:
    register_task(update, context, "cleaning")


@command
def meeting(update: Update, context: CallbackContext) -> None:
    register_task(update, context, "meeting")


