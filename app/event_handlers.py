"""colletcion of callback functions for passive events, like user joined or photo sent"""

from telegram import Chat, ChatMember, Message, MessageEntity, Update, User
from telegram.ext import CommandHandler, Updater, PicklePersistence, Filters, CallbackContext

import helpers as h

from telegram.utils.helpers import escape_markdown

from constants import USERS, GET_ADMIN_FROM_API_REQUEST_COUNTDOWN


def keep_user_register(update: Update, context: CallbackContext) -> None:
    """sync username and user object on any action
    
    do not use in very large chats as there is no deleting from the register"""
    message = update.message
    users = set()
    convert_ChatMember_to_User = lambda chat_member: chat_member.user
    #TODO extract users from mentions
    # get user from every possible source
    try:
        users |= {update.effective_user}
    except Exception as e: pass
    try:
        users |= set(map(convert_ChatMember_to_User, message.new_chat_members))
    except Exception as e: pass
    try:
        users |= set(map(convert_ChatMember_to_User, message.left_chat_member))
    except Exception as e: pass
    try:
        users |= {message.forward_from}
    except Exception as e: pass
    try:
        users |= {update.edited_message.from_user}
    except Exception as e: pass
    try:
        record = GET_ADMIN_FROM_API_REQUEST_COUNTDOWN
        get_administrators_frequency = 10 # request this data with low frequency
        try: 
            if context.chat_data[record] >= get_administrators_frequency: 
                context.chat_data[record]
                users |= set(map(convert_ChatMember_to_User, message.chat.get_administrators()))
            else:
                context.chat_data[record] += 1
        except KeyError: # no record set if first time
            context.chat_data[record] = 0
    except Exception as e: pass

    users.discard(None)

    try:
        registered_users: set[User] = context.bot_data[USERS] 
    except KeyError: # first use
        context.bot_data[USERS] = set()
    registered_users |= users
