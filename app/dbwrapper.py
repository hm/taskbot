
import sqlite3
from itertools import repeat
from sqlite3 import Error as DBError
from sqlite3 import Row
from typing import Union, Optional, List, Dict, Set, Tuple # Literal

from constants import PERIODS, TASKS_DB_PATH

# db connections
task_db: sqlite3.Connection = sqlite3.connect(TASKS_DB_PATH,check_same_thread=False) 
#user_db: sqlite3.Connection = sqlite3.connect(USER_DB_PATH,check_same_thread=False) 


### TASK related queries
def insert_task_in_db(*, db: sqlite3.Connection = task_db, 
    task_table="task", task_type: str, chat_id: int, participants: List[int], 
    place_name: str, message_text: str, message_id: int, requesting_user_id: int) -> int:
    """insert task in db and returns the task id"""
   
    #TODO check existing
    db.execute(f"""INSERT INTO {task_table}
        (type, place, creation_message_text, creation_message_id, creation_chat_id, creation_request_by_id)
        VALUES
        (?,?,?,?,?,?)""",
        (task_type, place_name, message_text, message_id, chat_id, requesting_user_id))
    db.commit()
 
    task_id = db.execute("SELECT last_insert_rowid();").fetchone()[0]
    db.commit()
    insert_task_participants(task_id, participants)

    return task_id 


def insert_task_participants(task_id:int, participants:List[int], *,  db: sqlite3.Connection = task_db):
    participant_insertion = []
    for participant_id in participants:
        participant_insertion.append((task_id, participant_id))

    db.executemany(f"INSERT INTO participants(task_id, telegram_id) VALUES (?,?)", participant_insertion)
    db.commit()


def creation_confirm_message(task_id:int, confirmation_message_id:int, confirmation_message_text: str, 
                            *, db: sqlite3.Connection = task_db):
    """register the confirmation message by the bot in the inserted task"""
    # creation_confirm_message_id INT,
    if not task_id: # if not specified, try to put thet in the last task inserted. Dangerous.
        new_task_id = "MAX(id)" 
    else:
        new_task_id = task_id

    db.execute("""UPDATE task
    SET creation_confirm_message_id = ? , creation_message_text = ?
    WHERE id = ?
    """, (confirmation_message_id, confirmation_message_text, new_task_id))
    db.commit()


def task_id_from_message(original_message_id: int, original_chat_id: int, 
                        db: sqlite3.Connection=task_db) -> int:
    """returns the task id from a creation message. Returns -1 if the message"""
    task_id_query = db.execute("""
                        SELECT id 
                        FROM task
                        WHERE creation_message_id = ? AND
                        creation_chat_id = ? ;""",
                        (original_message_id, original_chat_id)
                    ).fetchone()
    db.commit()

    if task_id_query is not None: 
        task_id = task_id_query[0]
    else: # selected a message that is not a creation message or some wierd things with telegram as the group_id changing
        task_id = -1
        # raise ValueError("not a creation message")
        
    return task_id


def delete_task_in_db(*, deleting_message_id: int, deleting_chat_id: int, deleting_message_text: str, 
                         original_message_id: int=0, original_chat_id: int=0, deletion_request_by_id: int, 
                         task_id: int=0, db:sqlite3.Connection=task_db) -> int:
    """mark deleted (update) in db the task specified.
    
    you can specify a task by referring by task_id or by the creation message.
    returns the id of the task deleted""" 

    db.execute("""                          
        UPDATE task                         
        SET deleted = TRUE, 
            deletion_timestamp = CURRENT_TIMESTAMP,                
            deletion_time = CURRENT_TIME,   
            deletion_date = CURRENT_DATE,   
            deletion_message_text = ? ,          
            deletion_message_id = ? ,       
            deletion_chat_id = ? ,
            deletion_request_by_id = ?             
        WHERE (creation_message_id = ? AND  
            creation_chat_id = ?)           
            OR id = ? ;""",
        (deleting_message_text, deleting_message_id, deleting_chat_id, deletion_request_by_id,
        original_message_id, original_chat_id, task_id))
    db.commit()

    #fetch the id of the last task deleted 
    if not task_id:
        task_id: int = task_id_from_message(original_message_id=original_message_id, 
                                   original_chat_id=original_chat_id)

    return task_id 
    

# PLACE related queries
def place_from_group_id(group_id: int, *, db=task_db) -> str:
    result = db.execute("""SELECT place 
                        FROM places_group_id  
                        WHERE telegram_group_id = ? 
                        ORDER BY inserted DESC 
                        LIMIT 1""", 
                        (group_id,)
            ).fetchone()
    db.commit()
    place_name = result[0] if result else ""
    
    return place_name


def insert_new_group(*, group_id:int, place:str, db=task_db) -> bool:

    db.execute("""
        INSERT INTO places_group_id 
        (place, telegram_group_id) 
        VALUES (?,?);""", 
        (place, group_id)
    )
    db.commit()


def list_places(db=task_db) -> List[str]:
    place_rows: List[Row] =  db.execute("""
            SELECT DISTINCT place 
            FROM places_group_id
            """).fetchall()
    db.commit()
    return [place[0] for place in place_rows] # Row unpacking


# DB meta queries
def dump(*, db=task_db) -> List[str]:
    return list(db.iterdump())


def get_db_table_names(*, db=task_db) -> List[str]:
    table_names_rows: List[Row] = db.execute("""
        select name from sqlite_master where type = 'table';""").fetchall()
    
    return [name[0] for name in table_names_rows]


def get_table_content(name:str, *, db=task_db) -> List[Tuple[str]]:
    records: List[Row] = db.execute(f"select * from {name} ;").fetchall()
    
    return records #TODO correct type casting


def get_table_columns(table_name, *, db=task_db) -> List[str]:
    columns_query_result: Tuple[Row] = db.execute(f"""
        PRAGMA table_info({table_name})""")

    return [column[0] for column in columns_query_result]


# PEOPLE related queries
def get_top_contributors(   top_n: int, period: str, # Literal['day','week','month','year','all'], # *PERIODS], 
                            last_n_periods:int, 
                            bounties_only:bool=False, *,db=task_db)  -> Dict[str,str]:
    
    is_sunday = bool(db.execute("SELECT DATE('now') = DATE('now', 'weekday 0')").fetchone()[0])
    # if is_monday:
    #    do_console(garfield)
    period_base_date = {
        'day':      "'now', 'start of day'",
        'week':     "'now', 'weekday 0'" + ("" if is_sunday else ", '-7 days'"),  # start of week
        'month':    "'now', 'start of month'",
        'year':     "'now', 'start of year'",
    }

    if period != 'all':
        if last_n_periods == 0: # today , this month etc...
            end_date = "'now'"
            begin_date = period_base_date[period]
        else:
            end_date = period_base_date[period] + "'-1 day'"
            begin_date = end_date + (f", '-{last_n_periods} {period}'" if period != 'week' else \
                                     f", '-{last_n_periods*7} day'")
        date_condition = f"""   AND task.creation_date >= (SELECT DATE({begin_date})) 
                                AND task.creation_date <= (SELECT DATE({end_date}))""" 
    else:
        date_condition = "" 
    
    bounties_join = "JOIN bounties ON task.id=bounties.id" if bounties_only else ""
    query = f"""
        SELECT telegram_id, COUNT(telegram_id)
        FROM participants 
            JOIN task 
                ON task.id=participants.task_id
            {bounties_join}
        WHERE task.deleted = FALSE
            {date_condition}
        GROUP BY telegram_id
        ORDER BY COUNT(telegram_id) DESC
        LIMIT ?
    ;"""
    query_results = db.execute(query, (top_n,))

    return dict(query_results)

