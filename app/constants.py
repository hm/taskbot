from telegram import MessageEntity
from typing import Dict

#types
PARSED_MESSAGEENTITIES = Dict[MessageEntity, str]

#paths
CONFIG_FILE_PATH = 'app/config.json'
TASKS_DB_PATH = 'database/task.sqlite3'
USER_DB_PATH = 'database/user.sqlite3'

#runtime constants
ANY_MENTION = [MessageEntity.MENTION, MessageEntity.TEXT_MENTION]
PERIODS = ['day','week','month','year','all'] #TODO put in constants, shared with text_commands
USERNAME_TO_USEROBJ_MAP = "username_to_userobj_map" #TODO transfer in config or constants
USERS = "userobj_set"
GET_ADMIN_FROM_API_REQUEST_COUNTDOWN = "get_administrators_countdown" # in context/chat_data[]



